#include <iostream>
#include <time.h>
#include <algorithm>
#include <vector>
#include "Deck.h"
#define SIZE 52
#define FACELIM 15
#define SUITLIM 5

//NOTE THAT FACELIM = 15 ASSUMING THAT ACE = 14, but if ACE = 13, then set FACELIM = 14!

using namespace std;

void SetDeck(vector<Deck>&);
void ShuffleDeck(vector<Deck>&);
void DealDeck(vector<Deck>&, vector<Deck>&, vector<Deck>&, vector<Deck>&, vector<Deck>&, int, int);
void HandScores(vector <Deck>&, vector<Deck>&, vector<Deck>&, vector<Deck>&, int);
void PrintDeckText(vector<Deck>&, int, string);

int main()
{
    vector<Deck> Card (SIZE);
    vector<Deck> P (13);
    vector<Deck> Q (13);
    vector<Deck> R (13);
    vector<Deck> S (13);

    SetDeck(Card);
    PrintDeckText(Card, SIZE, "Deck");
    srand(time(NULL));
    ShuffleDeck(Card);
    PrintDeckText(Card, SIZE, "Shuffled Deck");

    DealDeck(Card, P, Q, R, S, SIZE, 13);

    PrintDeckText(P, 13, "Player P");
    PrintDeckText(Q, 13, "Player Q");
    PrintDeckText(R, 13, "Player R");
    PrintDeckText(S, 13, "Player S");

    HandScores(P, Q, R, S, 13);

    return 0;
}

void SetDeck(vector<Deck>& Acard){
    int s = 1, i = 0;
    int f;
    while (s < SUITLIM){
        for (f = 2; i < SIZE && f < FACELIM; i++, f++){
            Acard[i].setsuit(s);
            Acard[i].setface(f);
        }
        s++;
    }
}

void ShuffleDeck(vector<Deck>& Acard){
    random_shuffle(Acard.begin(), Acard.end());
}

void DealDeck(vector<Deck>& Acard, vector<Deck>& Ahand, vector<Deck>& Bhand, vector<Deck>& Chand, vector<Deck>& Dhand, int DeckSize, int HandSize){
    int i, s;
    for (i = 0, s = 0; i < HandSize, s < DeckSize; i++, s += 4){
        Ahand[i].setsuit(Acard[s].getsuit());
        Ahand[i].setface(Acard[s].getface());
        Bhand[i].setsuit(Acard[s+1].getsuit());
        Bhand[i].setface(Acard[s+1].getface());
        Chand[i].setsuit(Acard[s+2].getsuit());
        Chand[i].setface(Acard[s+2].getface());
        Dhand[i].setsuit(Acard[s+3].getsuit());
        Dhand[i].setface(Acard[s+3].getface());
    }
}

void HandScores(vector<Deck>& Ahand, vector<Deck>& Bhand, vector<Deck>& Chand, vector<Deck>& Dhand, int HandSize){
    int AhandScore = 0, BhandScore = 0, ChandScore = 0, DhandScore = 0;
    int i;
    for (i = 0; i< HandSize; i++){
        AhandScore = AhandScore + Ahand[i].getsuit() + Ahand[i].getface();
        BhandScore = BhandScore + Bhand[i].getsuit() + Bhand[i].getface();
        ChandScore = ChandScore + Chand[i].getsuit() + Chand[i].getface();
        DhandScore = DhandScore + Dhand[i].getsuit() + Dhand[i].getface();
    }
    cout << "Dealer's (P) Score: " << AhandScore << "\nQ Hand's Score: " << BhandScore
    << "\nR Hand's Score: " << ChandScore << "\nS Hand's Score: " << DhandScore
    << "\n" <<endl;

    if (AhandScore > BhandScore && AhandScore > ChandScore && AhandScore > DhandScore){
        cout << "Dealer is the Winner!" << endl;
    }else
    if (BhandScore > AhandScore && BhandScore > ChandScore && BhandScore > DhandScore){
        cout << "Player Q is the Winner!" << endl;
    }else
    if (ChandScore > AhandScore && ChandScore > BhandScore && ChandScore > DhandScore){
        cout << "Player R is the Winner!" << endl;
    }else
    {
        cout << "Player S is the Winner!" << endl;
    }
}

void PrintDeckText(vector<Deck>& Acard, int DeckSize, string Player){
    cout << Player << "'s hand:\n________________________________________________________\n" << endl;
    int i;
    for (i = 0; i < DeckSize; i++){
        if (Acard[i].getface() < 11){
            cout << Acard[i].getface() << " of ";
        }else{
        switch (Acard[i].getface()){
            case 11:
                cout << "Jack of ";
                break;
            case 12:
                cout << "Queen of ";
                break;
            case 13:
                cout << "King of ";
                break;
            case 14:
                cout << "Ace of ";
                break;
        }
        }
        switch (Acard[i].getsuit()){
            case 1:
                cout << "Clubs" << endl;
                break;
            case 2:
                cout << "Diamonds" << endl;
                break;
            case 3:
                cout << "Hearts" << endl;
                break;
            case 4:
                cout << "Spades" << endl;
                break;
        }
    }
    cout << "________________________________________________________\n" << endl;
}
