#ifndef DECK_H
#define DECK_H


class Deck
{
    public:
        Deck();
        void setface(int);
        void setsuit(int);
        int getface();
        int getsuit();
    private:
        int face;
        int suit;
};

#endif // DECK_H
