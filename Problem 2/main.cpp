#include <iostream>
#include <time.h>
#include <algorithm>
#include <vector>
#include "Deck.h"
#define SIZE 104
#define FACELIM 15
#define SUITLIM 5

//NOTE THAT FACELIM = 15 ASSUMING THAT ACE = 14, but if ACE = 13, then set FACELIM = 14!

using namespace std;

void SetDeck(vector<Deck>&);
void ShuffleDeck(vector<Deck>&);
void DealDeck(vector<Deck>&, vector<Deck>&, vector<Deck>&, vector<Deck>&, vector<Deck>&,
              vector<Deck>&, vector<Deck>&, vector<Deck>&, vector<Deck>&, int, int);
void HandScores(vector <Deck>&, vector<Deck>&, vector<Deck>&, vector<Deck>&,
                vector<Deck>&, vector<Deck>&, vector<Deck>&, vector<Deck>&, int);
void PrintDeckText(vector<Deck>&, int, string);

int main()
{
    vector<Deck> LargeDeck (SIZE);
    vector<Deck> P (13);
    vector<Deck> Q (13);
    vector<Deck> R (13);
    vector<Deck> S (13);
    vector<Deck> T (13);
    vector<Deck> U (13);
    vector<Deck> V (13);
    vector<Deck> W (13);

    SetDeck(LargeDeck);
    PrintDeckText(LargeDeck, SIZE, "Large Deck");
    srand(time(NULL));
    ShuffleDeck(LargeDeck);
    PrintDeckText(LargeDeck, SIZE, "Shuffled Large Deck");

    DealDeck(LargeDeck, P, Q, R, S, T, U, V, W, SIZE, 13);

    PrintDeckText(P, 13, "Player P");
    PrintDeckText(Q, 13, "Player Q");
    PrintDeckText(R, 13, "Player R");
    PrintDeckText(S, 13, "Player S");
    PrintDeckText(T, 13, "Player T");
    PrintDeckText(U, 13, "Player U");
    PrintDeckText(V, 13, "Player V");
    PrintDeckText(W, 13, "Player W");

    HandScores(P, Q, R, S, T, U, V, W, 13);
    return 0;
}

void SetDeck(vector<Deck>& Acard){
    int s, f;
    int i = 0;
    while (i < SIZE){
        s = 1;
        while (s < SUITLIM){
            for (f = 2; i < SIZE && f < FACELIM; i++, f++){
                Acard[i].setsuit(s);
                Acard[i].setface(f);
            }
            s++;
        }
    }
}

void ShuffleDeck(vector<Deck>& Acard){
    random_shuffle(Acard.begin(), Acard.end());
}

void DealDeck(vector<Deck>& Acard, vector<Deck>& Ahand, vector<Deck>& Bhand, vector<Deck>& Chand, vector<Deck>& Dhand,
              vector<Deck>& Ehand, vector<Deck>& Fhand, vector<Deck>& Ghand, vector<Deck>& Hhand, int DeckSize, int HandSize){
    int i, s;
    for (i = 0, s = 0; i < HandSize, s < DeckSize; i++, s += 8){
        Ahand[i].setsuit(Acard[s].getsuit());
        Ahand[i].setface(Acard[s].getface());
        Bhand[i].setsuit(Acard[s+1].getsuit());
        Bhand[i].setface(Acard[s+1].getface());
        Chand[i].setsuit(Acard[s+2].getsuit());
        Chand[i].setface(Acard[s+2].getface());
        Dhand[i].setsuit(Acard[s+3].getsuit());
        Dhand[i].setface(Acard[s+3].getface());
        Ehand[i].setsuit(Acard[s+4].getsuit());
        Ehand[i].setface(Acard[s+4].getface());
        Fhand[i].setsuit(Acard[s+5].getsuit());
        Fhand[i].setface(Acard[s+5].getface());
        Ghand[i].setsuit(Acard[s+6].getsuit());
        Ghand[i].setface(Acard[s+6].getface());
        Hhand[i].setsuit(Acard[s+7].getsuit());
        Hhand[i].setface(Acard[s+7].getface());

    }
}

void HandScores(vector<Deck>& Ahand, vector<Deck>& Bhand, vector<Deck>& Chand, vector<Deck>& Dhand,
                vector<Deck>& Ehand, vector<Deck>& Fhand, vector<Deck>& Ghand, vector<Deck>& Hhand, int HandSize){
    int AhandScore = 0, BhandScore = 0, ChandScore = 0, DhandScore = 0;
    int EhandScore = 0, FhandScore = 0, GhandScore = 0, HhandScore = 0;
    int i;
    for (i = 0; i< HandSize; i++){
        AhandScore = AhandScore + Ahand[i].getsuit() + Ahand[i].getface();
        BhandScore = BhandScore + Bhand[i].getsuit() + Bhand[i].getface();
        ChandScore = ChandScore + Chand[i].getsuit() + Chand[i].getface();
        DhandScore = DhandScore + Dhand[i].getsuit() + Dhand[i].getface();
        EhandScore = EhandScore + Ehand[i].getsuit() + Ehand[i].getface();
        FhandScore = FhandScore + Fhand[i].getsuit() + Fhand[i].getface();
        GhandScore = GhandScore + Ghand[i].getsuit() + Ghand[i].getface();
        HhandScore = HhandScore + Hhand[i].getsuit() + Hhand[i].getface();
    }
    cout << "Dealer's (P) Score: " << AhandScore << "\nQ Hand's Score: " << BhandScore
    << "\nR Hand's Score: " << ChandScore << "\nS Hand's Score: " << DhandScore
    << "\nT Hand's Score: " << EhandScore << "\nU Hand's Score: " << FhandScore
    << "\nV Hand's Score: " << GhandScore << "\nW Hand's Score: " << HhandScore
    << "\n" << endl;

    if (AhandScore > BhandScore && AhandScore > ChandScore && AhandScore > DhandScore
        && AhandScore > EhandScore && AhandScore > FhandScore && AhandScore > GhandScore && AhandScore > HhandScore){
        cout << "Dealer is the Winner!" << endl;
    }else
    if (BhandScore > AhandScore && BhandScore > ChandScore && BhandScore > DhandScore
        && BhandScore > EhandScore && BhandScore > FhandScore && BhandScore > GhandScore && BhandScore > HhandScore){
        cout << "Player Q is the Winner!" << endl;
    }else
    if (ChandScore > AhandScore && ChandScore > BhandScore && ChandScore > DhandScore
        && ChandScore > EhandScore && ChandScore > FhandScore && ChandScore > GhandScore && ChandScore > HhandScore){
        cout << "Player R is the Winner!" << endl;
    }else
    if (DhandScore > AhandScore && DhandScore > BhandScore && DhandScore > ChandScore
        && DhandScore > EhandScore && DhandScore > FhandScore && DhandScore > GhandScore && DhandScore > HhandScore){
        cout << "Player S is the Winner!" << endl;
    }else
    if (EhandScore > AhandScore && EhandScore > BhandScore && EhandScore > ChandScore
        && EhandScore > DhandScore && EhandScore > FhandScore && EhandScore > GhandScore && EhandScore > HhandScore){
        cout << "Player T is the Winner!" << endl;
    }else
    if (FhandScore > AhandScore && FhandScore > BhandScore && FhandScore > ChandScore
        && FhandScore > DhandScore && FhandScore > EhandScore && FhandScore > GhandScore && FhandScore > HhandScore){
        cout << "Player U is the Winner!" << endl;
    }else
    if (GhandScore > AhandScore && GhandScore > BhandScore && GhandScore > ChandScore
        && GhandScore > DhandScore && GhandScore > EhandScore && GhandScore > FhandScore && GhandScore > HhandScore){
        cout << "Player V is the Winner!" << endl;
    }else
    if (HhandScore > AhandScore && HhandScore > BhandScore && HhandScore > ChandScore
        && HhandScore > DhandScore && HhandScore > EhandScore && HhandScore > FhandScore && HhandScore > GhandScore){
        cout << "Player W is the Winner!" << endl;
    }
}

void PrintDeckText(vector<Deck>& Acard, int DeckSize, string Player){
    cout << Player << "'s hand:\n________________________________________________________\n" << endl;
    int i;
    for (i = 0; i < DeckSize; i++){
        if (Acard[i].getface() < 11){
            cout << Acard[i].getface() << " of ";
        }else{
        switch (Acard[i].getface()){
            case 11:
                cout << "Jack of ";
                break;
            case 12:
                cout << "Queen of ";
                break;
            case 13:
                cout << "King of ";
                break;
            case 14:
                cout << "Ace of ";
                break;
        }
        }
        switch (Acard[i].getsuit()){
            case 1:
                cout << "Clubs\t" << endl;
                break;
            case 2:
                cout << "Diamonds\t" << endl;
                break;
            case 3:
                cout << "Hearts\t" << endl;
                break;
            case 4:
                cout << "Spades\t" << endl;
                break;
        }
    }
    cout << "________________________________________________________\n" << endl;
}
